# R image with plenty of packages installed

The image starts from `rocker/r-ver:4.1.3` and installs a (long) list of R packages commonly used in the D4Science environment.
