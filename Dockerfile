FROM rocker/r-ver:4.1.3

LABEL org.opencontainers.image.licenses="GPL-2.0-or-later" \
      org.opencontainers.image.source="https://github.com/rocker-org/rocker-versioned2" \
      org.opencontainers.image.vendor="Rocker Project" \
      org.opencontainers.image.authors="Carl Boettiger <cboettig@ropensci.org>"

# Some R dependencies
RUN apt-get update && apt-get install -y \
    jags \
    libxml2 \
    git \
    gdal-bin \
    libgdal-dev \
    libsodium-dev \
    libudunits2-dev \
    libfontconfig1-dev \
    netcdf-bin \
    libcairo2-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    sshpass \
    p7zip-full \
    sox \
    imagemagick \
    proj-bin \
    proj-data \
    libgsl23 \
    postgresql-server-dev-all \
    libsecret-1-0

# Install the R packages we need, using the binary distribution
# at https://packagemanager.rstudio.com/client/#/repos/1/overview
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/install2r-packages.txt /tmp
RUN for f in `cat /tmp/install2r-packages.txt` ; do install2.r --error --skipinstalled --ncpus -1 $f ; done

# Non CRAN repositories
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/r_non_cran_pkgs.txt /tmp
RUN for l in `cat /tmp/r_non_cran_pkgs.txt` ; do \
    pkg=`echo $l | cut -d : -f 1` ; \
    mirror=`echo $l | cut -d : -f 2-` ; \
    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
    -e "install.packages(pkgs='$pkg', repos=c('$mirror/'));" ; done

# Install TMB from sources
RUN pkg='TMB' ; \
    mirror='https://cloud.r-project.org' ; \
    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
    -e "install.packages(pkgs='$pkg', repos=c('$mirror/'));"

# From github
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/r_github_pkgs.txt /tmp
RUN mkdir -p /etc/R
RUN echo "GITHUB_PAT=$GITHUB_PAT" >> /etc/R/Renviron.site
#RUN for g in `cat /tmp/r_github_pkgs.txt` ; do \
#    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
#    -e " require(devtools); devtools::install_github('$g')" ; done
#RUN for g in `cat /tmp/r_github_pkgs.txt` ; do installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest $g ; done
#RUN grep -v "GITHUB_PATH" /etc/R/Renviron.site > /etc/R/Renviron.site.1
#RUN mv -f /etc/R/Renviron.site.1 /etc/R/Renviron.site
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest DanOvando/sraplus
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest sofia-tsaf/SOFIA
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest jabbamodel/JABBA
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest tokami/lbmsmt

# Ensure that we remove the source packages
RUN rm -fr /tmp/downloaded_packages/*

